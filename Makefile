.PHONY: all clean

all:
	make -C ECO201
	make -C HIS102

clean:
	make -C ECO201 clean
	make -C HIS102 clean
