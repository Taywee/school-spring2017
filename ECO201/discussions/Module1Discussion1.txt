1:
The book Economics Today, by Roger Miller, describes an "opportunity cost" as
the cost of a choice, measured in terms of the next-best choice that is
sacrificed as a result of making that choice. [1, p. 26]  In this way, it
appears that the "cost" of a decision is measured in analogy, because a choice's
value is impossible to directly measure in objective units, the "cost" of making
that choice is instead described as an opportunity that is given up in order to
make that choice.  In other words, the term "opportunity cost" can be taken
almost literally in order to understand the intended meaning.  Simply put, an
"opportunity cost" is the price of a choice, measured in units of an opportunity
that is "spent" in order to make the choice.  When you make a decision, it costs
you an opportunity.

One could make an argument that every decision has not one opportunity cost, but
infinitely many, as in taking a first choice, you are sacrificing not only the
second choice, but the third choice as well, and every other choice you can't
make because of the first choice.  While this is literally true, it doesn't
capture the spirit of an opportunity cost, which Economics Today fails to fully
explain.  An opportunity cost isn't an attempt to consider unmade choices, but
rather an attempt to estimate the value of a choice by considering what the
maker of the choice is willing to give up for it.

2:

I have made an uncountable amount of opportunity costs, as has everybody else.
I have made the choice to work the job I have, giving up the opportunity for an
earlier degree.  I have made the choice to pursue the degree I have, giving up
the opportunity for a mathematics or music theory degree.  As an interesting
example, I have made the choice to write this discussion post as early as I had
the time to, so that I wouldn't have to write it at the last minute Wednesday
night.  In this choice, I have sacrificed the opportunity to do some evening
work which would have made my work day tomorrow easier, which would have been my
next choice.  I have sacrificed an easier work day tomorrow in order to have a
less stressful evening.

[1] Miller, Roger LeRoy. Economics Today: The Macro View. 16th ed. Boston, MA: Addison-Wesley, 2012.
