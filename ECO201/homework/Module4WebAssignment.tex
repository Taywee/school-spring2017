\documentclass[12pt,letterpaper]{report}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[authordate,backend=biber]{biblatex-chicago}
\usepackage[margin=1in]{geometry}
\usepackage{soul}
\usepackage{parskip}
\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{hyphenat}
\title{Analyzing Money Measurements}
\date{2017-04-07}
\author{Taylor C. Richberger}

\makeatletter
\def\cms@choose{cms}
\makeatother

\bibliography{Module4WebAssignment}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\headheight = 15pt
\doublespacing

\begin{document}
    \makeatletter
    \begin{titlepage}
        \centering
        \vspace*{2cm}
        {\scshape\large \@title \par}
        \vspace{5cm}
        {\large \@author \par}
        \vspace{5cm}
        {\large Principles of Macroeconomics \par}
        {\large Professor Julia Walker \par}
        {\large \@date \par}
    \end{titlepage}
    \makeatother

    Money is an incredibly important concept for economics.  When most people
    think of economics, they think money.  While the basic concepts of economics
    have generalized quite a bit to encompass the use and organization of
    virtually all human decision-making, it is still primarily useful for
    measuring, predicting, and optimizing the flow and use of money.  Money
    itself, therefore, must be defined.  There are multiple measurements of
    money, differing in what is considered to be money.  These primarily come
    about because of the practice of ``fractional-reserve banking,'' which
    describes a system of banking in which banks hold less money than the total
    deposit amounts (so the money deposited is loaned out).  One of the primary
    differences in definitions of money is whether or not the money held in
    savings accounts is counted as separate from the money which is loaned out
    (which may, and usually does, draw from that savings account pool).
    \autocite[p.~331]{economicstoday}.

    The narrowest definition of the money supply used in the United States is
    known as \textbf{M1}.  M1 is the money supply defined as cash currency in
    circulation (including coins), traveler's checks not issued by banks, and
    transactions deposit accounts (deposit accounts which may have transactions
    directly issued from them---typically checking accounts or any other
    debitable account) \autocite[p.~322]{economicstoday}.  Essentially, M1 is a
    ``transactions approach'' to money measurement, referring to all money which
    may be directly and immediately spent as a part of a purchase.  There is
    also a ``liquidity approach'' to money.  This is an expanded pool, referred
    to as \textbf{M2}.  M2 is all of M1, plus savings accounts, small time
    deposits, and money market mutual funds.  This is called a ``liquidity
    approach'' because it expands on M1 and includes, on top of money that can
    be immediately spent, money that can be very quickly converted into a
    transactable form \autocite[p.~323]{economicstoday}.  Another money
    measurement is \textbf{MZM}, or money-at-zero-maturity.  MZM is similar to
    M2, but excludes time deposits (which require maturities) and includes all
    money market funds \autocite[p.~323]{economicstoday}.

    Data can be accessed for the Federal Reserve of the US in the way most
    public economic data can be accessed.  One of the useful stores of data is
    the FRED, the Federal Reserve Economic Data, provided by the Federal Reserve
    Bank of St. Louis \autocite{fed}.  The previously-mentioned measures of
    money can be cataloged and examined over a course of many decades.

    As far as major trends go, most of the measurements---M1 \autocite{m1},
    demand deposits \autocite{demand},
    and checkable deposits \autocite{checkable}---follow a very similar pattern, with steady growth
    up from the 60s to some rocky peaks and dips and general plateauing through
    the 80s, followed by a large growth up to 1995, hitting a peak of \$1.15
    trillion for M1.  During this period, Demand Deposits shrunk slightly
    between 1981 and 1983, but maintained sync with the other transaction
    measures afterward. These then all shrank slightly up to 2001.  M1 grew up
    to 2005, while demand deposits and checkable deposits stagnated, but M1
    started stagnating after this, staying roughly at \$1.4 trillion to August
    2008.  During this stagnation of M1, currency still continued to climb
    \autocite{currency},
    while checkable deposits and demand deposits both dropped about \$40
    billion.  All measures grew steadily from 2008 to present day.  Through the
    whole period, currency climbed steadily, with only a small stagnation
    between middle 2007 and middle 2008, and no general decline.  Many of these
    trends align with some major events.  Notably, the decline in transactable
    funds from 1994 through 2001 aligns with the dot-com bubble and the massive
    investment put into it.  This stayed roughly the same until the recession
    and afterward, with large growth of transactable money afterward.

    Minor trends show primarily a couple interesting landmarks.  There was a dip
    in demand deposits with a growth in general liquid money in early 1981,
    leading up to the recession in the early 1980s, likely due to people moving
    money out of savings accounts.  One obvious spike in the graphs is in
    September 2001, where M1 and all checkable and demand deposits spiked by
    over \$50 billion, and dropped back to their previous level afterward.  This
    was clearly a result of the September 11th attacks and the impending war,
    leading people to nervously move money where it could be readily accessed.
    Another very notable blip is in December 2008.  Though already in the throes
    of recession, the blip could have been families spending the stimulus checks
    awarded by the ``Economic Stimulus Act of 2008'' in April for Christmas that
    year, as there is no large spike in April \autocite{stim}.  Later, there was
    a growth spike by over \$100 billion between July and August 2011, likely
    relating to the stock market drop, and the downgrading of the US credit
    rating \autocite{creditrating}, causing people to liquidate assets.

    Travelers checks are far less exciting \autocite{travelers}.  They had grown
    up to a small plateau of \$4.2 billion during the 1980s recession, then grew
    steadily up to  \$9.2 billion in 1995.  After this, they dropped steadily to
    \$8.4 billion in 1997, where they stayed until roughly 2000, and then
    dropped steadily to \$2.1 billion currently.  The growth of travelers checks
    largely is in line with the general expansion of fluid currency up until the
    mid 90s.  After that, they largely fell out of favor, likely because ATMs
    are more common, and it's easier to make direct account transactions with
    modern credit and debit cards.  Fewer modern banks accept travelers checks
    as well, due to the disuse in favor of electronic means of obtaining cash or
    entirely avoiding its use altogether in favor of some form of electronic
    transactions.  Though still a part of M1 and M2, travelers checks are much
    less fluid and take more work to put money into, and are largely facing
    disuse, and are therefore less useful for monetary data, and will likely
    become moreso until they are completely disused.

    % Travelers checks: Steady growth to $9.2B in June 1995, steady decline to
    % $2.1B now.

    \printbibliography
\end{document}
