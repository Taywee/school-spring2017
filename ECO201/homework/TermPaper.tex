\documentclass[12pt,letterpaper]{report}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[authordate,backend=biber]{biblatex-chicago}
\usepackage[margin=1in]{geometry}
\usepackage{soul}
\usepackage{parskip}
\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{hyphenat}
\hyphenation{Zoll-verein}
\title{The Economic Effects of the Zollverein}
\date{2017-03-31}
\author{Taylor C. Richberger}

\makeatletter
\def\cms@choose{cms}
\makeatother

\bibliography{TermPaper}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\headheight = 15pt
\doublespacing

\renewcommand\thesection{\arabic{section}}

\begin{document}
    \makeatletter
    \begin{titlepage}
        \centering
        \vspace*{2cm}
        {\scshape\large \@title \par}
        \vspace{4cm}
        {\large \@author \par}
        \vspace{4cm}
        {\large Principles of Macroeconomics \par}
        {\large Professor Julia Walker \par}
        {\large \@date \par}
    \end{titlepage}
    \makeatother

    Free trade is generally agreed by Economists to be good for economies which
    practice it.  Free trade encourages more rapid technological progress,
    industrialization, and economic growth \autocite[p.~196]{economicstoday}.  In
    present day, not many countries have universal free trade, but most
    countries have free trade with at least one other country, due to free-trade
    agreements \autocite[p.~961]{makingofthewest}.  In particular, the European
    Union practices free trade among EU members, with a shared currency among
    most members (the Euro).  So despite not having truly universal free trade,
    most countries enjoy (and benefit politically from) free trade with a number
    of other nations.  This has not always been the case, however.  In the
    seventeenth and eighteenth centuries, most countries had high tariffs on
    most imports, and even afterwards, during the nineteenth century's
    industrialization, the biggest industrial nations had large tariffs, out of
    fear of cheap imports hurting domestic industry
    \autocite[p.~674]{makingofthewest}.

    It was not only an era of political and industrial change, but also an era
    of economic change.  The most notable economic change of this period was the
    German economic unification, known as the \textit{Zollverein} (Customs
    Union).  As the German states were still disparate political entities, The
    Zollverein resembled to some degree the present European Union from an
    economic point.  The Zollverein and the underlying concepts and treaties
    helped bolster German technology and economies by reducing trade barriers,
    reducing tolls, increasing motivation to innovate through trust in the
    currencies, and increasing spread of technology and innovation.  On the
    other hand, the Zollverein was not entirely beneficial, with its
    protectionist policies injuring innovation and growth which depended on
    foreign trade, particularly in the larger states, such as Prussia, where
    ``the protective duties of the Zollverein [were] particularly injurious''
    \autocite{princesmith}.

    The Zollverein came formally into existence in 1934.  Though it was slow to
    gain traction, and didn't seem to have much effect for over a decade, it
    slowly garnered acceptance among German states, with more and more states
    continually joining.  These economic changes were most beneficial to small
    states with very specialized economies \autocite{economicdevelopment}.
    These small states were able to use their specialization as an advantage,
    and also benefit from the industrial strengths of the larger states, such as
    Prussia \autocite{politicaleconomy}.  One of the biggest economic boosts of
    the Zollverein (other than war with Austria in 1866) was the Vienna Monetary
    Treaty, which consolidated all the states in the Zollverein into a common
    currency, and the partner states of Austria and Lichtenstein
    \autocite{sollderzollverein}.

    The Zollverein was not without detractors and critics, however.  One of the
    most famous critics was John Prince Smith, who had fought his entire life
    for free trade \autocite{princesmith}.  These critics had strong issue with
    the particularly protectionist policies of the Zollverein.  The Zollverein
    helped smaller member states contained within by enabling them access to the
    industries of the larger states, and enabling them to export their
    (primarily agricultural) goods to the larger states without the expensive
    tariffs and duties, but it did little to help the larger states, such as
    Prussia, which needed especially to be able to partake in external trade in
    order to improve its economic prosperity, and was ``more completely
    sacrificed than any other state by the protective system'' \autocite
    {princesmith}.  Also encouraging to these activists was the free trade
    movement in Britain of 1846, and its success in bolstering the British
    economy.  A large victory for free trade in Germany was won in 1860, with
    protectionist duties removed in favor of moderate duties for revenue
    purposes (this was carried out through the 1860s).

    \begin{figure}
        \caption{German exports, 1834-1889 (current values, million Marks).  It
        should be noted that the figures in this graph, and most German economic
        figures of the era, are not fully reliable, and not all German states
        kept good documentation of their economics.}
        \includegraphics[width=\textwidth]{exports.jpg}
        \autocite{tradestatistics}
    \end{figure}

    The economic effects of the Zollverein itself are also very difficult to
    estimate, as it is difficult to maintain any ceteris paribus conditions.
    This was a time period and area of vast economic change and poor economic
    records.  There was a multitude of wars, vastly changing economic
    conditions, constant expansion and imperial policies, rising and falling of
    empires, and continual liberal reform.  The shocks of economic changes in
    this period were largely coinciding with massive political changes.  Other
    factors were various regional differences within German states leading to
    less interstate trade. In other words, political and legislative policies
    and changes may have hindered economic expansion that would have otherwise
    taken place.  The Zollverein was positive, but the degree to which it was
    actually beneficial is incredibly hard to accurately estimate.  It is very
    likely that economic unification was heavily dampened even with the lack of
    duties and tariffs, compared to the potential gains of true political
    unification.

    An interesting side-effect of the Zollverein was the visibility of access to
    markets.  Distance from market goods and trade partners caused large
    divergence of economic growth within nations.  Growth of urbanization and
    general prosperity caused cities to grow larger near active markets, and
    smaller near protectionist borders \autocite{economicdevelopment}.  This
    demonstrates quite well the economic principle that tariffs reduce economic
    growth by raising the cost of business and reducing incentive to increase
    trade \autocite[p.~197]{economicstoday}.  Essentially, tariffs and
    protectionist policies act as market barriers, artificially inflating prices
    and offsetting the equilibrium of supply and demand, by lowering both supply
    and demand of import goods by raising price for both parties.  This
    underscores the economic importance of basic access for a nation's economy.
    Two of the most fundamental aspects of economic growth are access to
    resources with which goods may be produced, and access to markets through
    which those goods may be traded, and free international trade is one tool to
    facilitate both of those.

    Because of these barriers, Prussian business in particular exerted strong
    pressure on external customs liberalization in order to take advantage of
    the industrial strengths of nearby nations, such as France and Britain.
    Prussia sought international trade treaties with as many neighboring
    countries as it possibly could to stimulate a stronger economy
    \autocite{freetrade}.  Prussia was already benefiting from the ``buoyant
    export market for primary sector goods'' \autocite{economicdevelopment}.
    One of the interesting effects of the Zollverein internally as a result of
    the protectionism is that prices within Zollverein nations came closer to
    convergence for many Zollverein states, particularly for wheat and rye.

    Germany's primary weakness was its economic and political fragmentation.
    While the Zollverein was a vast step forward, and an interesting exception
    to the usual case of political unification preceding economic unification
    \autocite{politicaleconomy}, it still did not surmount all of the obstacles
    in its path.  The Zollverein didn't provide full economic unification
    because of the barriers of political differences and the costs of the
    traditionalist conservative systems in place, and it still failed to provide
    the industrial and economic prosperity that Britain had enjoyed (largely due
    to its own more-successful free trade movements).  Because the smaller
    states already had a much smaller pool of resources, they benefited more
    vastly from the customs union, enabling them to reap the rewards of the
    greater pool available to them, but the larger states needed open trade
    borders to benefit, as they were already the economic and resource
    powerhouses, with the least to gain from free internal trade, and the most
    to gain from free external trade with other economically powerful nations.
    One thing that the Zollverein has emphasized is that political disunity
    creates a barrier in economic growth.  Economic growth may proceed most
    smoothly with as few barriers as possible to that economy.  This is
    facilitated by lack of protectionist tariffs, a common currency, ready
    access to markets and resources, and is helped by a common language and lack
    of political barriers.

    \printbibliography
\end{document}
