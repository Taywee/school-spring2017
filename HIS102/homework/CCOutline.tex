\documentclass[12pt,letterpaper]{report}
\usepackage[margin=1in]{geometry}
\usepackage{soul}
\usepackage{parskip}
\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{setspace}
\title{Conflict of Enlightenment and Romantic\\Ideals in Mozart's \textit{Die Zauberfl\"{o}te}}
\date{2017-03-05}
\author{Taylor C. Richberger}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\headheight = 15pt
\doublespacing

\renewcommand\thesection{\arabic{section}}

\begin{document}
    \makeatletter
    \begin{titlepage}
        \centering
        \vspace*{2cm}
        {\scshape\large \@title \par}
        \vspace{4cm}
        {\large \@author \par}
        \vspace{4cm}
        {\large Western Civilization: 1650-Present \par}
        {\large Professor Reanne Eichele \par}
        {\large \@date \par}
    \end{titlepage}
    \makeatother

    \begin{center}
        Outline
    \end{center}

    \section{Thesis}

    \textit{Die Zauberfl\"{o}te} is undeniably a literary conglomerate, with
    many elements and themes lifted from other works and fairy tales, as well as
    actual events of the time. Through these lifted themes and tales---whether
    intentionally or otherwise---Mozart and Schikaneder aptly express and
    summarize many of the existing conflicts of ideals and movements of the era.
    The play may ultimately be an argument for compromise of ideals.

    \section{Outline}

    \begin{enumerate}
        \item Introduction, thesis, short history, short description of opera,
            describing some characters, themes as they progress, and thematic
            shifts.  Some small focus on the thematic shift between acts, but
            leave detail and discussion out for later.

        \item Summary of the types of themes portrayed in the opera, along with
            historical origins of specific themes (much of the story was
            borrowed from prior works).  Specific origins of themes such as the
            Brotherhood (stone masonry), the birdcatcher, the flute, the queen
            of the night, and other symbolism.

        \item Treatment of women in the play, how the treatment of women
            contradicts itself in many parts.
            \begin{enumerate}
                \item Tie into use of sexuality in the play, and discuss the way
                    in which sexuality is alternatively alluded to, demonized,
                    and idealized, based on the origins of the themes.

                \item Pamina is sympathised with, and is treated as an object of
                    idealized love (Tomino) and demonized lust Monostatos). For
                    much of the play, Pamina does little but react, with very
                    little active character action, but in the end, she plays an
                    active role, leading Tamino to his goal.  Is this an
                    empowering portrayal, or a reaffirmation of a woman's role
                    as being a helper of her husband, ultimately an accessory of
                    his quest?

                \item Bring in the contradiction between Sarastro and the Queen
                    of the Night, and their switching roles between the acts.
                    Mostly contrast Queen of the Night and Sarastro in their
                    sex, not by their other thematic elements.  Also bring in
                    Pamina's father, how he is related to Sarastro, and his
                    relationship to the Queen of the Night.

                \item Touch on the science vs nature aspect of Queen of the
                    Night vs Sarastro, so it can be used to transition into the
                    following section.
            \end{enumerate}

        \item Science vs Nature.
            \begin{enumerate}
                \item Discuss the presence of Stonemasonry as an obvious theme,
                    and how it contradicts with the more natural aspects of the
                    play (particularly the first act).

                \item Discuss Sarastro as a symbol of the sun, tied with
                    science, illumination, and enlightenment, and  the Queen of
                    the moon as a representation of dangerous, dark, ignorant
                    nature.

                \item On the other hand, the queen is sympathised with in the
                    first act, and science is treated as confusing and dangerous
                    through much of the second act, and the Mason-like rites are
                    treated as seemingly-ineffectual in the second act.

                \item In the end, Tamino's romance is justified by his reason,
                    but also as a conquering of the elements through the flute.
                    Also discuss the sun literally vanquishing the night in the
                    long run (definitely a symbol of reason conquering
                    ignorance, but to what end?  Is it an admonishment of
                    Romanticism, or a simple defense of enlightenment?)
            \end{enumerate}

        \item Short discussion of the instruments.
            \begin{enumerate}
                \item What is the ultimate purpose of the instruments, other
                    than to propel the story?

                \item Do the flute and bells have agency?

                \item Do they represent something more distilled than that, and
                    actually portray the goals of some pure ideal or purpose?

                \item Is there a single goal that can be deduced from the
                    functioning of each instrument, or even both together, and
                    how does that tie into the ideals of the opera and of the
                    time?
            \end{enumerate}

        \item Discussion of the hard shift between the two between the two acts.

            \begin{enumerate}
                \item Researched discussion into the possibilities why the shift
                    happened, whether it was intentional for thematic reasons or
                    if it may have a more detailed history.

                \item The shift itself is stark and immediately noticeable.  The
                    first half of the play is fantastical, very fairy-tale like,
                    and filled with romanticism.  The second act shifts
                    immediately, defending Sarastro, and immediately turning the
                    Queen into a villain.

                \item Tamino's trials are treated as confusing, but ultimately
                    necessary to his enlightenment.  In the end, his romance is
                    still treated as noble, but only when tempered with reason.
                    Is this a compromise or an argument for Enlightenment?
            \end{enumerate}

        \item Discussion of Papageno. This paragraph might be dismantled and
            strewn into some of the other paragraphs if I can't make it strong
            enough to stand on its own.

            \begin{enumerate}
                \item I want to break down Papageno, his contrast to Tamino, and
                    the ideals and perspectives that he portrays.
                \item Particularly, I'd like to bring in Papageno not just as a
                    comedic relief, but an actual audience perspective.
                \item Papageno is not just an oaf, and his goals do not fail in
                    the end.  He is a weak, flawed, human contrast to the pure
                    ideals as portrayed in the play.
                \item He is a symbol of the fact that pure idealism frequently
                    does not and can not exist in its purest form, and the fact
                    that perhaps all these conflicting ideals don't need to be
                    conflicting.
                \item In Tamino's and Pamina's apparent joining of Romantic and
                    Enlightenment ideals at the end, providing an argument for
                    compromise of ideals, Papageno may be an argument for lack
                    of necessity of strong, conflicting ideals, as he also
                    achieves his happiness despite not representing any strong
                    ideal.
                
            \end{enumerate}

        \item Conclusion, summarizing previously mentioned points, tying into
            author's own conjecture and views on the themes of the play,
            arguments made through the characters (focusing on subtler
            arguments, rather than explicitly stated points made in arias), and
            specifically how Papageno carries a theme of moderation and
            simplicity between the other extreme opposing ideologies contrasting
            and conflicting through the course of the opera.  In the end, who
            had achieved their goals and why?  What does their success or
            failure convey for the themes expressed in the play?
    \end{enumerate}
\end{document}
