\documentclass[12pt,letterpaper]{report}
\usepackage[margin=1in]{geometry}
\usepackage{soul}
\usepackage{parskip}
\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{setspace}
\title{Conflict of Enlightenment and Romantic\\Ideals in Mozart's \textit{Die Zauberfl\"{o}te}}
\date{2017-03-05}
\author{Taylor C. Richberger}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\headheight = 15pt
\doublespacing

\begin{document}
    \makeatletter
    \begin{titlepage}
        \centering
        \vspace*{2cm}
        {\scshape\large \@title \par}
        \vspace{4cm}
        {\large \@author \par}
        \vspace{4cm}
        {\large Western Civilization: 1650-Present \par}
        {\large Professor Reanne Eichele \par}
        {\large \@date \par}
    \end{titlepage}
    \makeatother

    \begin{center}
        Annotated Bibliography
    \end{center}

    \hangindent=0.5in
    \hangafter=1
    Tettlebaum, Marianne. ``Whose Magic Flute?'' \textit{Representations} 102,
    no. 1 (2008): 76-93. doi:10.1525/rep.2008.102.1.76.
    \url{http://www.jstor.org.libdb.ppcc.edu/stable/pdf/10.1525/rep.2008.102.1.76.pdf}

    \ul{The question of who owns the flute, and therefore whose will is
    expressed through the magic flute, has a strong effect on the
    representation of the opera, and therefore its relationship to German
    enlightenment.}  The flute is played by Tamino, but it was not his flute
    initially, and was granted to him by the primary malicious figure in the
    play.  The flute was granted by the Queen of the Night, but was carved by
    Pamina's father, and notably during a thunderstorm.  The agency of the flute
    can be argued to be either Tamino's, Pamina's Father's (who is linked
    to Sarastro), the Queen of the Night's, or even that of the gods themselves.
    This agency can be said to influence which character's intentions are being
    expressed during the opera, and can also be seen to change the
    interpretation of the opera between pro-Enlightenment or pro-Romanticism.
    It may even be suggested that the agency of the flute changes based on
    interpretation and production of the play.  Indeed, different productions
    use different creative decisions to put more emphasis on different aspects
    of the play, even to the point of emphasizing the natural (Romantic)
    aspects, Enlightenment aspects, or political aspects of the opera.  The
    article argues thusly that the flute belongs to none of the characters, but
    to the audience themselves.  The flute belongs to the listener and the
    critic, and as such, we all are ``flute-players''.  Ultimately, the flute is
    linked to Plato's \textit{Symposium} which references flute-playing.  The
    flute in both works is asserted to be rhetorical.
    [JSTOR]

    \hangindent=0.5in
    \hangafter=1
    Coser, Rose Laub. ``The Principle of Patriarchy: The Case of `The Magic
    Flute' '' \textit{Signs} 4, no. 2 (1978): 337-48.
    \url{http://www.jstor.org.libdb.ppcc.edu/stable/pdf/3173030.pdf}

    \ul{\textit{The Magic Flute} portrays some apparent disdain for women, and
    despite modern cinema's attempts at cleansing those influences, it is
    impossible to remove them given the plot of the story, which appears to
    represent a feminine lack of control giving way to a masculine wisdom.}  The
    article discusses the apparent detachment between the first act and the
    second, with the first portraying the Queen of the Night as a caring mother
    and a figure of warmth and protection, and Sarastro as a cruel tyrant.  This
    is reversed in the second act, when the Queen is suddenly vengeful and
    Sarastro is a wise, caring, and bold ruler, who wishes to induct Tamino into
    his Mason-like brotherhood.  This representation portrays the patriarchal
    attitudes of maturity, with the mother caring and protecting a child early
    in life (the serpent may be a symbol of the dangers of the world) to a
    father taking over in adolescence to shape the child's wisdom.  It is
    asserted that Sarastro's ultimate wisdom and the virtues of his brotherhood
    are portrayed as the ultimate boons of masculinity.  Indeed, Tamino's love
    may only be true and valid after being tempered by the wisdom and reason of
    Sarastro's brotherhood.  Despite a modern representation making Sarastro
    into Pamina's father, even in the original, Sarastro's relation to Pamina's
    father can not be ignored, and the final scene with Sarastro joining Tamino
    and Pamina can be easily likened to a father giving away his daughter.  The
    article argues as well that the flute is phallic in nature, and a
    representation of phallic, masculine power.  It is argued that the play
    portrays the only power capable of making change as being  a ``powerful
    phallus''.
    [JSTOR]

    \hangindent=0.5in
    \hangafter=1
    Spaethling, Robert. ``Folklore and Enlightenment in the Libretto of Mozart's
    Magic Flute.'' \textit{Eighteenth-Century Studies} 9, no. 1 (1975): 45-68.
    doi:10.2307/2737659.
    \url{http://www.jstor.org.libdb.ppcc.edu/stable/pdf/2737659.pdf}

    \ul{The libretto (text) of \textit{The Magic Flute} is a general
    conglomerate of local fairy tales, Enlightenment ideals, literary references
    and clich\'{e}s, and poetic banalities, making it a literary amalgam of many
    different, even conflicting, ideals of the time}.  The play is argued to be
    a ``literary conglomerate'', with the article even arguing that the play is
    rife with contradictions, thematic issues, and inconsistencies.  Many of the
    origins are discussed, with the existence of the magical flute coming from a
    contemporary fairy tale, \textit{Lulu oder die Zauberfl\"{o}te}, which
    contains a lost prince, a fairy queen, a magical flute, and a magical
    glockenspiel.  An educational book set in Egypt was the source of many of
    the Masonic elements of the opera, with the Sarastro's trials coming almost
    word for word from it.  Papageno is argued to be a dual-role, being a
    ``parrot'' (``Papagei'' is German for ``parrot''), a joking mimic of
    Tamino's quest, and a sexual symbol, playing the pan-pipe, like the satyrs
    of Greek myth.  Many other connections to myth are present as well, such as
    Tamino and Pamina's purification through the elements, proving innocence and
    purity, and ultimately mastering nature.  Much of these old symbols are
    linked to masonic and Enlightenment ideals of conquoring darkness and
    ignorance with wisdom, with the Queen being seen as a symbol of old
    ignorance (the cults of the Earthmother) and Isis and Osiris symbols of new
    reason, particularly with the symbolism of the sun, a common metaphor for
    light and reason.  This is made very clear (and quite on the nose) at the
    end of the play when the sun literally vanquishes the night itself and its
    queen.  This mirrors the basic ideas of the enlightenment being to restore
    the light after the darkness of the past.  In many ways, this massive
    combination of different ideas and themes makes the opera much of a mess,
    but in other ways also accounts for its longevity in the theater.  The play
    is undeniably messy and flawed, but survives through the wonderful music of
    Mozart, fascination with the amalgam of differing ideas, and interest in the
    history of the creation of the play.
    [JSTOR]


\end{document}
