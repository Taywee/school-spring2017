<html xmlns="http://www.w3.org/1999/xhtml">
  <body>
    <p>
    Primary sources and secondary sources have varying uses across varying
    fields.  A primary source is a contemporary source, written by somebody at
    the time who was, in general, directly experiencing an event.  A secondary
    source is somebody who is writing based on one or more primary sources.  In
    most fields, primary sources are more highly valued than secondary sources,
    as they can give insight not only into the events of the time, but also into
    reasoning and arguments as to why they were done, and the feelings and
    contention of the time.
    </p>
    <p>
    Where a secondary source, such as a history textbook, may say something such
    as &quot;Darwinism had evolved from a contribution to science to a racist
    justification for imperialism,&quot;<sup>[1]</sup> a primary source, such as
    Rudyard Kipling's <i>The White Man's Burden</i>, published 1899, gives a
    first-hand view into these thoughts.  This poem phrases the imperialism of
    the day as not only positive, but an absolute necessity, claiming that white
    imperialists were, at their own detriment, benefiting to conquered nations,
    contributing culture, language, medicine, and unearned blame, calling the
    conquered people &quot;Half-devil and half-child&quot;<sup>[2]</sup>
    </p>
    <p>
    This view wasn't rare, either, with many people feeling their imperialist
    country was the &quot;soldier of civilization.&quot;  In Alaxandre
    Millerand's letter on such, he argues against revolutionary socialism,
    Millerand touches on these topics, but also on an other important conflict
    of the time, the conflict between reform socialism and revolutionary
    socialism.  A secondary source may touch on the details of the conflict, but
    only through a primary source may we see the ferventness with which people
    argued for these ideas.  Millerand arguments that revolutionary socialists
    are betraying their ideals, and that reformist socialism is the true path to
    a socialist utopia.  These emotion and logic-driven arguments are things
    that are missing from nearly any secondary source.  Importantly, a secondary
    source may try to explain the ideas of the time, but Millerand's writing is
    not an explanation, but a pointed argument, an attempt to persuade others to
    his believed ideals.  The power of his conviction can be seen in passages
    such as &quot;Words do not frighten me; but I dread equivocations. And what
    equivocation could be more unfortunate than that of a party masked by a
    title which contradicts formally its spirit and its method? If we reckon
    violence reprehensible as well as useless, if legal reform appear to us at
    once as our immediate objective and as the sole practical procedures to
    bring us nearer our distant goal, let us, then, have the courage, not a
    difficult courage, to call ourselves by our own name, "reformists," since
    reformists we are.&quot;<sup>[3]</sup>
    </p>
    <p>
    One further weakness of a secondary source is that it can not hit the
    emotional depth of a person's experiences.  People living in horrifying
    conditions, people fighting for ideas they don't necessarily agree with, and
    people struggling to survive.  These can be told exceptionally through a
    secondary source, but only through a primary source may we possibly see
    through the eyes, the experiences of a person who has been in those
    conditions.  British imperialism in India is frequently told from a
    political and economic level, but famed writer George Orwell recounts
    experiences living in Burma in his story &quot;Shooting an Elephant&quot;.
    In the story, Orwell is forced by his own hand to kill an elephant which has
    killed a man, but has ceased to be a threat.<sup>[4]</sup>  The story
    becomes a metaphor for the dangers of imperialism, pushing the idea that
    imperialism not only destroys the conquered, but the conquerer as well,
    because a conquering imperial may not waver from their goal, as the only
    thing maintaining their presence and superiority is their unrelenting
    strength and dominance.  As well as a metaphor, the story provides the
    reader insight for life that is hardly considered, that of occupying
    officers in an imperialist colony.
    </p>
    <p>
    I have written extensively in support of primary sources, but is there a
    weakness to them, compared to secondary sources?  I would argue that one
    significant weakness of primary sources is bias.  Where secondary sources
    tend to focus on documentation and summarization (though they are definitely
    not free from bias themselves), many primary sources are attempting to
    persuade, or argue for an ideal.  Because a primary source is somebody
    living a situation, their bias can not possibly be distanced from the work,
    where a secondary source may compile differing and diverse viewpoints,
    gaining a more realistic and level view of events.  One such example of bias
    in a primary source is Jules Ferry's &quot;Speech before the French National
    Assembly&quot;, in which he argues for the necessity of French imperialism
    as a necessity for French economic well-being.<sup>[5]</sup>  In this piece,
    there are arguments for and against imperialism, but most of the
    anti-imperialist voices are drowned out by the speech which appears to have
    majority support.  Where this piece portrays heated arguments,
    interruptions, and pointed attacks (some of which may be personal), a
    secondary source of a more modern era can not only weigh these arguments
    against other ideas of the time, but also against the hindsight of the
    effects of these argument, further developments, and modern interpretations.
    </p>
    <p>
    Are primary sources better than secondary sources?  I would argue that they
    are for certain aspects.  Secondary sources are much more useful for
    compiled data, cross-analysis, and facts with a distance from emotionally
    charged arguments.  Primary sources, on the other hand, are useful for how
    people held and used ideas of the time, better understanding lived
    experiences of people given situations of the past, and obtaining a context
    for disagreements and conflict.  Without secondary sources, the details of
    events are vastly inefficient to compile, analyze, and understand.  Without
    primary sources, the ideas, emotions, and context of the time is impossible
    to fully realize.  No good piece of research in a social science is without
    at least one primary source, but secondary sources are far more useful tools
    for efficiently gaining a wide picture and carving a place for the context
    of those primary sources.
    </p>
    <hr/>
    <p>[1] Lynn Hunt, Thomas R. Martin, Barbara H. Rosenwein, Bonnie G. Smith.
    Making of the West, People and Cultures, third ed.(Boston: Bedford St
    Martin’s, 2009), 654.</p>
    <p>[2] Kipling, Rudyard. &quot;The White Man’s Burden, 1899.&quot; Internet
    History Sourcebooks. Accessed March 19, 2017.
    http://sourcebooks.fordham.edu/halsall/mod/kipling.asp</p>
    <p>[3] Millerand, Alexandre. &quot;Reformist Socialism, 1903.&quot; Internet
    History Sourcebooks. Accessed March 14, 2017.
    http://sourcebooks.fordham.edu/halsall/mod/1903blackburden.asp</p>
    <p>[4] Orwell, George. &quot;Shooting an Elephant.&quot; 1936, London, GB.
    Accessed March 19, 2017.
    https://ppcc.desire2learn.com/content/enforced2012/1772957-S_PPCC_HIS1021N1_201730/George%20Orwell%20Shooting%20an%20Elephant.pdf</p>
    <p>[5] Ferry, Jules. &quot;The Speech Before the National Assembly.&quot;
    Modern Imperialism: Western Overseas Expansion and Its Aftermath, 1776–1965
    (1969): 69–74.
    https://ppcc.desire2learn.com/content/enforced2012/1772957-S_PPCC_HIS1021N1_201730/Jules%20Ferry.pdf</p>
  </body>
</html>
